<?php

namespace App\Http\Controllers;

use App\Models\Kabupaten;
use Illuminate\Http\Request;

class KabupatenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Kabupaten::select('id_kab', 'id_prov', 'nama')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_kab' => 'required',
            'id_prov' => 'required',
            'nama' => 'required'
        ]);
        Kabupaten::create($request->post());
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Kabupaten $kabupaten
     * @return \Illuminate\Http\Response
     */
    public function show(Kabupaten $kabupaten)
    {
        return response()->json([
            'kabupaten' => $kabupaten
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Kabupaten $kabupaten
     * @return \Illuminate\Http\Response
     */
    public function edit(Kabupaten $kabupaten)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Kabupaten $kabupaten
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kabupaten $kabupaten)
    {
        $request->validate([
            'id_kab' => 'required',
            'id_prov' => 'required',
            'nama' => 'required'
        ]);
        $data = $request->except('_method');
        Kabupaten::where('id_kab', $kabupaten['id_kab'])->update($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Kabupaten $kabupaten
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kabupaten $kabupaten)
    {
        $kabupaten->delete();
    }
}
