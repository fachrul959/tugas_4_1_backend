<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    use HasFactory;
    public $table = 'kelurahan';
    protected $fillable = ['id_kec', 'id_kel', 'nama'];
    protected $primaryKey = 'id_kel';
    public $timestamps = false;
}
