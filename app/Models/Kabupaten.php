<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kabupaten extends Model
{
    use HasFactory;
    public $table = 'kabupaten';
    protected $fillable = ['id_prov', 'id_kab', 'nama'];
    protected $primaryKey = 'id_kab';
    public $timestamps = false;
}
