<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    use HasFactory;
    public $table = 'provinsi';
    protected $fillable = ['id_prov', 'nama'];
    protected $primaryKey = 'id_prov';
    public $timestamps = false;
}
