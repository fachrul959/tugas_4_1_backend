<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    use HasFactory;
    public $table = 'kecamatan';
    protected $fillable = ['id_kec', 'id_kab', 'nama'];
    protected $primaryKey = 'id_kec';
    public $timestamps = false;
}
